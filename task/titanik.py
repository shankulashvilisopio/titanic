import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    prefixes = ["Mr.", "Mrs.", "Miss."]

    result = []

    for item in prefixes:
        title_df = df[df['Name'].str.contains(item)]

        median_age = round(title_df['Age'].median())

        missing_values = title_df['Age'].isnull().sum()

        result.append((item, missing_values, median_age))

    print(result)

    return None

print(get_filled())



